package Pages;

import Constants.PhpTravelsConstants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import util.TestBase;

public class PhpTravelsPage extends TestBase implements PhpTravelsConstants {

    @FindBy(xpath = "/html/body/header/div[2]/div/a/img")
    WebElement phpTravelsLogo;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[1]/span/span/a")
    WebElement demo;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[2]/span/span/a")
    WebElement pricing;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[3]/span")
    WebElement features;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[4]/span/span/a")
    WebElement product;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[5]/span/span/a")
    WebElement hosting;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[6]/span/span/a")
    WebElement services;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[7]/span/span/a")
    WebElement company;
    @FindBy(xpath = "//*[@id='main-menu']/ul/li[2]/span/span/a")
    WebElement blog;
    @FindBy(id = "//*[@id='main-menu']/ul/li[2]/span/span/a")
    WebElement login;

    public PhpTravelsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void validateUserNavigatedToPhpTravelsPage() {
        //Assert.assertTrue(phpTravelsLogo.isDisplayed());
        System.out.println("User in validateUserNavigatedToPhpTravelsPage"); /*
        verifyObjectIsDisplayed(phpTravelsLogo);
        verifyObjectIsDisplayed(demo);
        verifyObjectContainsText(demo, PhpTravelsConstants.demo);
        verifyObjectIsDisplayed(pricing);
        verifyObjectContainsText(pricing, PhpTravelsConstants.pricing);
        verifyObjectIsDisplayed(features);
        verifyObjectContainsText(features, PhpTravelsConstants.features); */
    }
}