package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static WebDriver driver = null;
    public static Properties properties;
    static int PAGE_lOAD_TIMEOUT = 20;
    static int IMPLICIT_WAIT = 30;

    public static WebDriver initializeDriver() throws Exception {
        properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream("D:\\Automation\\Projects\\bddframework\\src\\main\\java\\config\\config.properties");
        properties.load(fileInputStream);

        String browserName = properties.getProperty("browser");
        System.out.println("Browser name is " + browserName);

        if (browserName.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            driver = new ChromeDriver();
            driver.get(properties.getProperty("url"));
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(PAGE_lOAD_TIMEOUT, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
        } else if (browserName.equals("IE")) {
            //launch Internet Explorer
        }
        return driver;
    }

    public static void tearDown() {
        driver.quit();
    }
}
