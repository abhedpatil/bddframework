Feature: PhpTravels Homepage All Objects Validations

  Scenario: validateAllObjectsOnPhpTravelsObjectsloadedsuccessfully
    Given userLaunchesChromeBrowserAndNavigatedToPhpTravelsHomePage
    Then validateAllObjectsOnPhpTravelsHomepageAreLoadedSuccessfully
    Then closeBrowser
