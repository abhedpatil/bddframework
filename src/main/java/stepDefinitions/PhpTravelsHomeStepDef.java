package stepDefinitions;

import Pages.PhpTravelsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import util.TestBase;

public class PhpTravelsHomeStepDef extends TestBase {

    PhpTravelsPage phpTravelsPage = new PhpTravelsPage(driver);

    @Given("^userLaunchesChromeBrowserAndNavigatedToPhpTravelsHomePage$")
    public void userlauncheschromebrowserandnavigatedtophptravelshomepage() throws Exception {
        System.out.println("Initializing driver");
        TestBase.initializeDriver();
    }

    @Then("^validateAllObjectsOnPhpTravelsHomepageAreLoadedSuccessfully$")
    public void validateallobjectsonphptravelshomepageareloadedsuccessfully() {
        phpTravelsPage.validateUserNavigatedToPhpTravelsPage();
    }

    @Then("^closeBrowser$")
    public void closebrowser() {
        TestBase.tearDown();
    }
}