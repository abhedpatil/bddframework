package TestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "D:\\Automation\\Projects\\bddframework\\src\\main\\java\\Feature\\PhpTravelsHomePageObjects.feature",
        glue = {"D:\\Automation\\Projects\\bddframework\\src\\main\\java\\stepDefinitions\\PhpTravelsHomeStepDef.java"},
        format = {"pretty", "html:test-outout"},
        monochrome = true,  //display console output in readable format
        strict = true, //check if step is not defined in step definition file
        dryRun = false //check mapping is correct between feature file and step definition file
)
public class TestRunner {

}